//pt a gasi daca ex diferente intre cont unui folder intre 2 rulari
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>

#define MAX_PATHL 500

char* statusFile; //fisierul in care retinem datele pt a compara
int functie; //VAL 1 PT PRIMA RULARE SI 2 PT URMATOARELE

char numeDirSafe[50]; //numele dir in care vom izola fis malitioase

void saveFolderStatus(int fd , char* folder)
{
  DIR* dir;
  struct dirent* entry;
  struct stat fileStat;
  int malitios = 0; //flag pt a stii care fis trebuie trecute in snapshot

  if( (dir = opendir(folder)) == NULL)
  {
      perror("Eroare la desch folderului");
      exit(EXIT_FAILURE);
  }
  //parcurgem folderul
  while( (entry = readdir(dir)) != NULL)
  {
    if( (strcmp(entry->d_name , ".") != 0) && (strcmp(entry->d_name , "..") != 0) )
    { // intrari care ne intereseaza
      char absPath[MAX_PATHL];
      sprintf(absPath , "%s/%s" , folder, entry->d_name);
      
      if( (stat(absPath , &fileStat)) == -1)
      {
        perror("Eroare la lstat");
        exit(EXIT_FAILURE);
      }

      //daca fisierul e 'malitios'(are dr 000 nu il punem in snapshot)
      //int acStatus;
      if( access(absPath , R_OK) == -1 && access(absPath , W_OK) == -1 && access(absPath , X_OK) == -1 )//daca toate sunt pe 0 verificam fisierul
      {
        malitios = 0; //flag pt a stii daca trecem sau nu intrarea in snapshot

        printf("NUMELE FISIERELOR FARA DREPTURI: %s\n" , absPath);
        //rulam scriptul pt a verifica continutul fis
        //mai facem un copil sa ruleze doar script
        int pid , status;
        //numele scriptului
        char* pathScript = "/home/dar1u/Desktop/so/so/Aplicatie4/detectMalware.sh";
        //ii dam permisiune de read pt a rula script in fiu
        if (chmod(absPath, S_IRUSR | S_IRGRP | S_IROTH) == 0) 
        {
          //printf("Permisiune de read cu succes.\n");
        } 
        else 
        {
          perror("Eroare la primirea permisiunii de read");
        }

        pid = fork();

        if(pid < 0)
        {
          perror("Eroare fork");
          exit(EXIT_FAILURE);
        }
        else if(pid == 0) //proces fiu, rulam scriptul
        {
          execlp("sh" , "sh" , pathScript , absPath , NULL);//rulam scriptul in procesul copil
          
          perror("eroare la execlp");
          exit(EXIT_FAILURE);

        }
        else
        {
          wait(&status); // asteptam sa se termine procesul copil
          //apoi ii scoatem dreptul de read inapoi
        
          struct stat file_stat;
          if (stat(absPath, &file_stat) == -1) {
            perror("Error getting file status");
          }

          // Scoatem dreptul de read pt owner, group, and others
          mode_t new_mode = file_stat.st_mode & ~(S_IRUSR | S_IRGRP | S_IROTH);

          //update cu noile permisiuni
          if (chmod(absPath, new_mode) == 0) {
              //printf("Read permission removed successfully.\n");
          } else {
              perror("Error in changing file permissions");                
          }

          
          if (WIFEXITED(status)) 
          {
            // Procesul copil s-a incheiat normal
            int exit_status = WEXITSTATUS(status);//pt a verifica codul cu care s-a incheiat
            //scriptul nostru(care ia locul procesului copil)
            
            //verificam codul si in caz ca e 0(a gasit ceva malitios), il mutam in directorul
            //de izolare
            if(exit_status == 0)
            {
              printf("Fisierul %s NU e malitios\n" , absPath);
            }
            else //e malitios, trebuie mutat
            {
              malitios = 1;//
              printf("Fisierul %s e MALITIOS---- ESTE MUTAT\n" , absPath);
              char pathToSafeDir[MAX_PATHL];

              sprintf(pathToSafeDir , "%s/%s" , numeDirSafe , entry->d_name);

              if (rename(absPath, pathToSafeDir) == 0) 
              {
                //printf("Fisier malitios mutat cu succes.\n");
              } 
              else 
              {
                perror("Eroare la mutarea fisierului malitios");
              }
            }
          } 
          else 
          {
            // Child process terminated abnormally
            printf("Child process terminated abnormally\n");
          }
        }
      }
      //gata faza cu filtrarea fisierelor
      
      if(malitios == 0)//e intrare nemalitioasa, o trecem in snapshot
      {
        if (write(fd, absPath , strlen(absPath)) == -1) 
        {
          perror("Eroare la scrierea pathului");
          exit(EXIT_FAILURE);
        }//pt a scrie pathul(nume complet)

        char sizeStr[20];
        char modifTimeStr[20];
        sprintf(sizeStr, "%ld", fileStat.st_size);
        sprintf(modifTimeStr, "%ld", fileStat.st_mtime);

        if (write(fd, " ", 1) == -1 || 
                write(fd, sizeStr, strlen(sizeStr)) == -1 || 
                write(fd, " ", 1) == -1 ||
                write(fd, modifTimeStr, strlen(modifTimeStr)) == -1) 
                {
                  perror("Error writing size or access time");
                  exit(EXIT_FAILURE);
                } //pt a scrie size si last access

        if (write(fd, "\n", 1) == -1)
        {
                  perror("Error writing newline");
                  exit(EXIT_FAILURE);
        }
      }
      //daca entry-ul este de tip director
      if(S_ISDIR(fileStat.st_mode))
        saveFolderStatus(fd , absPath );
    }
  }
  closedir(dir);
  printf("SNAPSHOT for folder %s created successfully\n" , folder);
}

void compareFolderToStatus(int fd , int fd2 , char* numeFolder) //functia pt un folder
{
  int i = 0;
  char buf[MAX_PATHL*2 + 50];
  ssize_t bytesRead = read(fd , buf , sizeof(buf));

  if(bytesRead == -1)
  {
    perror("Eroare la cit din fd");
    exit(EXIT_FAILURE);
  }

  while(buf[i] != '`')
    i++;
  buf[i] = '\0';
  //printf("%s\n\n\n" , buf);

  char buf2[MAX_PATHL*2 +50];
  bytesRead = read(fd2 , buf2 , sizeof(buf2));

  if(bytesRead == -1)
  {
    perror("Eroare la cit din fd2");
    exit(EXIT_FAILURE);
  }

  i = 0;
  while(buf2[i] != '`')
    i++;
  buf2[i]='\0';
  //printf("%s\n\n\n" , buf2);

  if(strcmp(buf , buf2) != 0)
    printf("~~~~SUNT MODIFICARI la fold: ~%s\n" , numeFolder);
  else
    printf("~~~~NU SUNT MODIFICARI la fold: ~%s\n" , numeFolder);
  
}

void scrieTerminatorFisier(int fd)
{
  char c = '`';
  write(fd , &c , 1);
}

void compareFoldersToStatus(char* numeFolder , int functie)
{
  //sa verificam daca numeFolder e folder
  char numeFisBaza[] = "";
  strcat(numeFisBaza , numeFolder);

  char numeFisStatus[100] = "";
  char numeFisStatus2[100] = "";//pt cele 2 fisiere create pt fiecare

  strcpy(numeFisStatus , numeFisBaza);
  strcpy(numeFisStatus2 , numeFisBaza);
  
 
  //TREBUIE SA FACEM FIS CU NUMELE FOLDERULUI PT A UPDATA DOAR ACEL FIS DACA REAPELAM
  //CU PARAM IN ALTA ORDINE
  char sufix[50] = "1.txt";
  char sufix2[50] = "2.txt";

  strcat(numeFisStatus , sufix);
  strcat(numeFisStatus2 , sufix2);

  //printf("%s\t%s\n" , numeFisStatus , numeFisStatus2);
 
  int fd1 , fd2;
  struct stat fileStat;

  if(functie == 1)
  {
    if( (fd1 = open(numeFisStatus , O_WRONLY | O_CREAT | O_TRUNC, 0644)) == -1)
    {
      perror("Er la desch fis de statusI ");
    }

    saveFolderStatus(fd1 , numeFolder);
    scrieTerminatorFisier(fd1);

    close(fd1);
  }
  else //a 2+ rulare
  {
    if( (fd1 = open(numeFisStatus , O_RDONLY )) == -1)
    {
      perror("Er la desch fis de statusI") ;
      exit(EXIT_FAILURE);
    }

    if( (stat(numeFisStatus2 , &fileStat)) == -1)//fis nu exista
    {
      if( (fd2 = open(numeFisStatus2 , O_WRONLY | O_CREAT | O_TRUNC, 0644)) == -1)//il cream
      {
        perror("Eroare la desch fisierului de statusF ");
        exit(EXIT_FAILURE);
      }
    }
    else
      if( (fd2 = open(numeFisStatus2 , O_WRONLY | O_TRUNC)) == -1)
      {
        perror("Eroare la desch fisierului de statusF ");
        exit(EXIT_FAILURE);
      }

    saveFolderStatus(fd2 , numeFolder);
    scrieTerminatorFisier(fd2);// salvam statusul actual in fd2
    close(fd2);

    //redesch pt citire
    if( (fd2 = open(numeFisStatus2 , O_RDONLY )) == -1)
    {
      perror("Eroare la desch fisierului de statusF");
      exit(EXIT_FAILURE);
    }

    compareFolderToStatus(fd1 , fd2 , numeFolder);
    close(fd1);
    close(fd2);
  }

  strcpy(numeFisStatus , "");
  strcpy(numeFisStatus2 , "");
  strcpy(sufix , "");
  strcpy(sufix2 , "");
}

int verifFolder(char* numeFolder)//pt a filtra din main doar arg care sunt nume de foldere
{
  char absPath[100] = "";
  sprintf(absPath , "./%s" , numeFolder);

  struct stat fileStat;

  if( (stat(absPath , &fileStat)) == -1)
    {
      perror("Eroare: nu exista folderul/fisierul dat ca param");
      exit(EXIT_FAILURE);
    }

  if(S_ISDIR(fileStat.st_mode))
    return 1;//e folder

  return 0;
}

int gasesteDir_S(char* argv[] , int nr)
{
  int i;
  for(i = 1 ; i<=nr ; i++)
    if( strcmp(argv[i] , "-s") == 0 )//gasit flag
    {
      i = i + 1;
      break;
    }
  
  if(i == nr+1)
    return -1;

  return i;
}

//Acum vrem ca fiec proces copil sa faca snapshot pt un folder
//4** Pt fiec file care nu are niciun drept(000) il verificam cu un script daca e 'malitios' 
//si il vom muta in folderul(folder de izolare) indicat de param dir(inca un param care vine dupa un flag -s)
int main(int argc , char* argv[])
{
  if(argc < 2)
  {
    perror("Dati argument numele folderului\n");
    exit(EXIT_FAILURE);
  }

 
  int nrFoldere = argc - 1;
  int i , functie;
  int pid;

  int indDir = gasesteDir_S(argv , nrFoldere);//poz la care se afla dir in care vom muta fisierele 'malitioase'
  //printf("%d %d\n" ,nrFoldere , indDir);
  if(indDir < 0)
  {
    perror("Nu ati introdus flagul -s si directorul in care se vor muta fisierele mal");
    exit(EXIT_FAILURE);
  }

  if( verifFolder(argv[indDir]) == 0) //nu e folder 
    perror("Argumentul dat dupa flagul -s nu este folder sau nu exista");
  
  strcpy(numeDirSafe , argv[indDir]);//var in care pastram numele dir safe

  
  printf("Introduceti 1 daca e prima rulare, 2 in caz contrar\n");
  scanf("%d" , &functie);


  for(i = 1; (i<=nrFoldere) ; i++)//sa nu apelam snapshotul pe flag si dir in care vom muta
  {
    //printf("%d\n" , i);
    if(i != indDir && i != indDir -1)
    {
      //verif daca fiecare arg e folder
      if( verifFolder(argv[i]))//e folder, facem ce ne cere
      {
        pid = fork();
        if(pid < 0)
        {
          perror("Eroare fork");
          exit(EXIT_FAILURE);
        }
        else if(pid == 0)//e un copil
        {
          compareFoldersToStatus(argv[i] , functie);
          exit(0);
        }
      }
    }
  }

  int statusWait;

  for(i=1 ; i<=nrFoldere - 2; i++)//
  {
    pid = wait(&statusWait);
    if(pid == -1)
    {
      perror("Eroare la wait");
      exit(EXIT_FAILURE);
    }

    if(WIFEXITED(statusWait))
    {// proces copil incheiat normal
      int exit_code = WEXITSTATUS(statusWait);
      printf("Child process %d with PID %d exited with code: %d\n", i , pid , exit_code);
    }
    else
      printf("Proces copil incheiat anormal\n");
  }
    
  
  
  /*
  char* folder = argv[1];//nume director
  statusFile = "status.txt";
  char* statusFile2 = "status2.txt";
  int fd , fd2;

  printf("Introduceti 1 daca e prima rulare, 2 in caz contrar\n");
  scanf("%d" , &functie);

  if(functie == 1)
  {
    if( (fd = open(statusFile , O_WRONLY | O_CREAT | O_TRUNC, 0644)) == -1)
    {
      perror("Eroare la desch fisierului de status");
      exit(EXIT_FAILURE);
    }
    saveFolderStatus(fd , folder);
    char c = '`';
    write(fd , &c , 1);
  }
  else
  {
    if( (fd = open(statusFile , O_RDONLY )) == -1)
    {
      perror("Eroare la desch fisierului de status");
      exit(EXIT_FAILURE);
    }
  
    if( (fd2 = open(statusFile2 , O_WRONLY | O_CREAT | O_TRUNC, 0644)) == -1)
    {
      perror("Eroare la desch fisierului de status2");
      exit(EXIT_FAILURE);
    }
    saveFolderStatus(fd2 , folder);
    
    char c = '`';
    write(fd2 , &c , 1);
    close(fd2);

    if( (fd2 = open(statusFile2 , O_RDONLY )) == -1)
    {
      perror("Eroare la desch al doilea fisierului de status");
      exit(EXIT_FAILURE);
    }
    compareFolderToStatus(fd , fd2);
  }

  close(fd);
  close(fd2);
  */
  return 0;
}




//pt a gasi daca ex diferente intre cont unui folder intre 2 rulari
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_PATHL 500

char* statusFile; //fisierul in care retinem datele pt a compara
int functie; //VAL 1 PT PRIMA RULARE SI 2 PT URMATOARELE

void saveFolderStatus(int fd , char* folder)
{
  DIR* dir;
  struct dirent* entry;
  struct stat fileStat;

  if( (dir = opendir(folder)) == NULL)
  {
      perror("Eroare la desch folderului");
      exit(EXIT_FAILURE);
  }
  //parcurgem folderul
  while( (entry = readdir(dir)) != NULL)
  {
    if( (strcmp(entry->d_name , ".") != 0) && (strcmp(entry->d_name , "..") != 0) )
    { // intrari care ne intereseaza
      char absPath[MAX_PATHL];
      sprintf(absPath , "%s/%s" , folder, entry->d_name);
      
      if( (stat(absPath , &fileStat)) == -1)
      {
        perror("Eroare la lstat");
        exit(EXIT_FAILURE);
      }

      if (write(fd, absPath , strlen(absPath)) == -1) 
      {
        perror("Eroare la scrierea pathului");
        exit(EXIT_FAILURE);
      }//pt a scrie pathul(nume complet)

      char sizeStr[20];
      char modifTimeStr[20];
      sprintf(sizeStr, "%ld", fileStat.st_size);
      sprintf(modifTimeStr, "%ld", fileStat.st_mtime);

      if (write(fd, " ", 1) == -1 || 
              write(fd, sizeStr, strlen(sizeStr)) == -1 || 
              write(fd, " ", 1) == -1 ||
              write(fd, modifTimeStr, strlen(modifTimeStr)) == -1) 
              {
                perror("Error writing size or access time");
                exit(EXIT_FAILURE);
              } //pt a scrie size si last access

      if (write(fd, "\n", 1) == -1)
      {
                perror("Error writing newline");
                exit(EXIT_FAILURE);
      }

      //daca entry-ul este de tip director
      if(S_ISDIR(fileStat.st_mode))
        saveFolderStatus(fd , absPath );
    }
  }
  closedir(dir);
}

void compareFolderToStatus(int fd , int fd2 , int indice) 
{
  int i = 0;
  char buf[MAX_PATHL*2 + 50];
  ssize_t bytesRead = read(fd , buf , sizeof(buf));

  if(bytesRead == -1)
  {
    perror("Eroare la cit din fd");
    exit(EXIT_FAILURE);
  }

  while(buf[i] != '`')
    i++;
  buf[i] = '\0';
  //printf("%s\n\n\n" , buf);

  char buf2[MAX_PATHL*2 +50];
  bytesRead = read(fd2 , buf2 , sizeof(buf2));

  if(bytesRead == -1)
  {
    perror("Eroare la cit din fd2");
    exit(EXIT_FAILURE);
  }

  i = 0;
  while(buf2[i] != '`')
    i++;
  buf2[i]='\0';
  //printf("%s\n\n\n" , buf2);

  if(strcmp(buf , buf2) != 0)
    printf("SUNT MODIFICARI la fold %d\n" , indice);
  else
    printf("NU SUNT MODIFICARI la fold %d\n" , indice);
  
}

void scrieTerminatorFisier(int fd)
{
  char c = '`';
  write(fd , &c , 1);
}

void compareFoldersToStatus(int indice , char* numeFolder , int functie)
{
  char numeFisBaza[] = "status";
  char numeFisStatus[100] = "";
  char numeFisStatus2[100] = "";//pt cele 2 fisiere create pt fiecare
  strcat(numeFisStatus , numeFisBaza);
  strcat(numeFisStatus2 , numeFisBaza);

  char sufix[50];
  char sufix2[50];
  sufix[0] = indice + '0';
  sufix2[0] = indice + '0';

  strcat(sufix , "1.txt");
  strcat(sufix2 , "2.txt");

  strcat(numeFisStatus , sufix);
  strcat(numeFisStatus2 , sufix2); //in numeFisStatus avem numele fis pe care il vom crea
  
  //printf("%s %s\n" , numeFisStatus , numeFisStatus2);
  int fd1 , fd2;
  struct stat fileStat;

  if(functie == 1)
  {
    if( (fd1 = open(numeFisStatus , O_WRONLY | O_CREAT | O_TRUNC, 0644)) == -1)
    {
      perror("Er la desch fis de statusI ");
    }

    saveFolderStatus(fd1 , numeFolder);
    scrieTerminatorFisier(fd1);

    close(fd1);
  }
  else
  {
    if( (fd1 = open(numeFisStatus , O_RDONLY )) == -1)
    {
      perror("Er la desch fis de statusI") ;
      exit(EXIT_FAILURE);
    }

    if( (stat(numeFisStatus2 , &fileStat)) == -1)//fis nu exista !! nu merge aici
    {
      if( (fd2 = open(numeFisStatus2 , O_WRONLY | O_CREAT | O_TRUNC, 0644)) == -1)
      {
        perror("Eroare la desch fisierului de statusF ");
        exit(EXIT_FAILURE);
      }
    
    }
    else
      if( (fd2 = open(numeFisStatus2 , O_WRONLY | O_TRUNC)) == -1)
      {
        perror("Eroare la desch fisierului de statusF ");
        exit(EXIT_FAILURE);
      }

    saveFolderStatus(fd2 , numeFolder);
    scrieTerminatorFisier(fd2);// salvam statusul actual in fd2
    close(fd2);

    //redesch pt citire
    if( (fd2 = open(numeFisStatus2 , O_RDONLY )) == -1)
    {
      perror("Eroare la desch fisierului de statusF");
      exit(EXIT_FAILURE);
    }

    compareFolderToStatus(fd1 , fd2 , indice);
    close(fd1);
    close(fd2);
  }

  strcpy(numeFisStatus , " ");
  strcpy(numeFisStatus2 , " ");
  strcpy(sufix , " ");
  strcpy(sufix2 , " ");
}

int main(int argc , char* argv[])
{
  if(argc < 2)
  {
    perror("Dati argument numele folderului\n");
    exit(EXIT_FAILURE);
  }
  //facem un str cu numele general si dupa facem -i.txt
  int nrFoldere = argc - 1;
  int i , functie;

  printf("Introduceti 1 daca e prima rulare, 2 in caz contrar\n");
  scanf("%d" , &functie);

  for(i = 1; i<=nrFoldere ; i++)
    compareFoldersToStatus(i , argv[i] , functie);
  //
  
  /*
  char* folder = argv[1];//nume director
  statusFile = "status.txt";
  char* statusFile2 = "status2.txt";
  int fd , fd2;

  printf("Introduceti 1 daca e prima rulare, 2 in caz contrar\n");
  scanf("%d" , &functie);

  if(functie == 1)
  {
    if( (fd = open(statusFile , O_WRONLY | O_CREAT | O_TRUNC, 0644)) == -1)
    {
      perror("Eroare la desch fisierului de status");
      exit(EXIT_FAILURE);
    }
    saveFolderStatus(fd , folder);
    char c = '`';
    write(fd , &c , 1);
  }
  else
  {
    if( (fd = open(statusFile , O_RDONLY )) == -1)
    {
      perror("Eroare la desch fisierului de status");
      exit(EXIT_FAILURE);
    }
  
    if( (fd2 = open(statusFile2 , O_WRONLY | O_CREAT | O_TRUNC, 0644)) == -1)
    {
      perror("Eroare la desch fisierului de status2");
      exit(EXIT_FAILURE);
    }
    saveFolderStatus(fd2 , folder);
    
    char c = '`';
    write(fd2 , &c , 1);
    close(fd2);

    if( (fd2 = open(statusFile2 , O_RDONLY )) == -1)
    {
      perror("Eroare la desch al doilea fisierului de status");
      exit(EXIT_FAILURE);
    }
    compareFolderToStatus(fd , fd2);
  }

  close(fd);
  close(fd2);
  */
  return 0;
}



